﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports EFAccess.MAVHAConnectionString


Public Class TODORules

	Dim context As EFAccess.MAVHAConnectionString = New EFAccess.MAVHAConnectionString()

	Public Function GetAllTODO() As IEnumerable(Of EFAccess.TODO)
		Dim allTODO = context.TODO.ToList()
		Return allTODO 'As IEnumerable(Of EFAccess.TODO)
	End Function

	Public Function GetAllTODO(ByVal id As Int32, ByVal descripcion As String, ByVal estado As String) As IEnumerable(Of EFAccess.TODO)
		Dim allTODO = context.TODO.Where(Function(x) x.Id = id Or (x.Descripcion = descripcion Or x.Estado = estado))
		Return allTODO 'As IEnumerable(Of EFAccess.TODO)
	End Function

	Public Function GetOneTODO(ByVal id As Int32) As EFAccess.TODO
		Return context.TODO.Where(Function(x) x.Id = id).First()
	End Function


	Public Sub SaveTODO(ByVal todo As EFAccess.TODO)
		context.TODO.Add(todo)
		context.Entry(context.TODO).State = System.Data.Entity.EntityState.Modified
		context.SaveChanges()
	End Sub

	Public Sub DeleteOne(ByVal id As Int32)
		Dim todo As EFAccess.TODO = GetOneTODO(id)
		context.TODO.Remove(todo)
		context.Entry(context.TODO).State = System.Data.Entity.EntityState.Deleted
		context.SaveChanges()
	End Sub


End Class
