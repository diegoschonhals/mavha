﻿Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports EFAccess


Namespace Controllers
    Public Class TODOController
        Inherits ApiController

		' GET: api/TODO
		Public Function GetAll() As IEnumerable(Of EFAccess.TODO)

			Try
				Dim rules As EFAccess.TODORules = New TODORules()
				Dim todoList As IEnumerable(Of EFAccess.TODO) = rules.GetAllTODO()
				'Return Request.CreateResponse(Of EFAccess.TODO)(HttpStatusCode.OK, todoList)
				Return todoList
			Catch ex As Exception
				Return New HttpResponseMessage(HttpStatusCode.BadRequest)
			End Try

		End Function

		' GET: api/TODO/5
		Public Function GetValue(ByVal id As Integer) As EFAccess.TODO
			Try
				Dim rules As TODORules = New TODORules()
				Return rules.GetOneTODO(id)
			Catch ex As Exception
				Return Nothing
			End Try
		End Function

		' POST: api/TODO
		Public Function PostValue(<FromBody()> ByVal todo As EFAccess.TODO) As HttpResponseMessage
			Try
				Dim rules As TODORules = New TODORules()
				rules.SaveTODO(todo)
				Return New HttpResponseMessage(HttpStatusCode.OK)
			Catch ex As Exception
				Return New HttpResponseMessage(HttpStatusCode.BadRequest)
			End Try
		End Function

		' PUT: api/TODO/5
		Public Function PutValue(ByVal id As Integer, <FromBody()> ByVal todo As EFAccess.TODO) As HttpResponseMessage
			Try
				Dim rules As TODORules = New TODORules()
				rules.SaveTODO(todo)
				Return New HttpResponseMessage(HttpStatusCode.OK)
			Catch ex As Exception
				Return New HttpResponseMessage(HttpStatusCode.BadRequest)
			End Try
		End Function

		' DELETE: api/TODO/5
		Public Function DeleteValue(ByVal id As Integer) As HttpResponseMessage
			Try
				Dim rules As TODORules = New TODORules()
				rules.DeleteOne(id)
				Return New HttpResponseMessage(HttpStatusCode.OK)
			Catch ex As Exception
				Return New HttpResponseMessage(HttpStatusCode.BadRequest)
			End Try

		End Function
	End Class
End Namespace